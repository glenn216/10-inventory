﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventory
{
    public partial class frmAddProduct : Form
    {
        private string _ProductName;
        private string _Category;
        private string _MfgDate;
        private string _ExpDate;
        private int _Quantity;
        private double _SellPrice;
        public BindingSource showProductList;
        public frmAddProduct()
        {
            InitializeComponent();
            showProductList = new BindingSource();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] ListOfProductCategory = 
            {
                "Beverages",
                "Bread/Bakery",
                "Canned/Jarred Goods",
                "Dairy",
                "Frozen Goods",
                "Meat",
                "Personal Care",
                "Other"
            };
            foreach(string element in ListOfProductCategory)
            {
                cbCategory.Items.Add(element);
            }
        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            try
            {
                _ProductName = Product_Name(txtProductName.Text);
                _Category = cbCategory.Text;
                _MfgDate = dtPickerMfgDate.Value.ToString("yyyy-MM-dd");
                _ExpDate = dtPickerExpDate.Value.ToString("yyyy-MM-dd");
                var _Description = richTxtDesciption.Text;
                _Quantity = Quantity(txtQuantity.Text);
                _SellPrice = SellingPrice(txtSellPrice.Text);
                showProductList.Add(new ProductClass(_ProductName, _Category, _MfgDate,
                _ExpDate, _SellPrice, _Quantity, _Description));
                gridViewProductList.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                gridViewProductList.DataSource = showProductList;
                MessageBox.Show("The product has been successfully added to the list.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (NumberFormatException)
            {
                MessageBox.Show("This is a NumberFormatException", "NumberFormatException", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            catch(StringFormatException)
            {
                MessageBox.Show("This is a StringFormatException", "StringFormatException", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch(CurrencyFormatException)
            {
                MessageBox.Show("This is a CurrencyFormatException", "CurrencyFormatException", MessageBoxButtons.OK ,MessageBoxIcon.Exclamation);
            }
            finally
            {
                txtProductName.Clear();
                cbCategory.SelectedIndex = -1;
                richTxtDesciption.Clear();
                txtQuantity.Clear();
                txtSellPrice.Clear();
            }
        }
        public string Product_Name(string name)
        {
            if (!Regex.IsMatch(name, @"^[a-zA-Z]+$"))
            {
                throw new StringFormatException("This is a StringFormatException.");
            }

            return name;
        }
        public int Quantity(string qty)
        {
            if (!Regex.IsMatch(qty, @"^[0-9]"))
            {
                throw new NumberFormatException("This is a NumberFormatException.");
            }

            return Convert.ToInt32(qty);
        }
        public double SellingPrice(string price)
        {
            if (!Regex.IsMatch(price.ToString(), @"^(\d*\.)?\d+$"))
            {
                throw new CurrencyFormatException("This is a CurrencyFormatException.");
            }

            return Convert.ToDouble(price);
        }
    }
    class NumberFormatException : Exception
    {
        public NumberFormatException(string varName) : base(varName) { }
    }
    class StringFormatException : Exception
    {
        public StringFormatException(string varName) : base(varName) { }
    }
    class CurrencyFormatException : Exception
    {
        public CurrencyFormatException(string varName) : base(varName) { }
    }

}
